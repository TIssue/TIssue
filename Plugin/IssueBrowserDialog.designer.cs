﻿namespace TIssuePlugin
{
    internal partial class IssueBrowserDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyURLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.companionsComboBox = new System.Windows.Forms.ComboBox();
            this.companionsLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tabControlPanel = new System.Windows.Forms.Panel();
            this.issueTabControl = new System.Windows.Forms.TabControl();
            this.issueTabPage = new System.Windows.Forms.TabPage();
            this.filterListView = new System.Windows.Forms.ListView();
            this.filterColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.issueListView = new System.Windows.Forms.ListView();
            this.idColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.titleColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelsColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.assignToColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.milestoneColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.descriptionColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.authorColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.issueSearchPanel = new System.Windows.Forms.Panel();
            this.issueStateComboBox = new System.Windows.Forms.ComboBox();
            this.issueStateLabel = new System.Windows.Forms.Label();
            this.regExpCheckBox = new System.Windows.Forms.CheckBox();
            this.searchComboBox = new System.Windows.Forms.ComboBox();
            this.searchFieldComboBox = new System.Windows.Forms.ComboBox();
            this.searchInLabel = new System.Windows.Forms.Label();
            this.searchLabel = new System.Windows.Forms.Label();
            this.previewTabPage = new System.Windows.Forms.TabPage();
            this.issuePrevCaptionPanel = new System.Windows.Forms.Panel();
            this.issuePrevCaptionLabel = new System.Windows.Forms.Label();
            this.issuePrevPanel = new System.Windows.Forms.Panel();
            this.descWebBrowser = new System.Windows.Forms.WebBrowser();
            this.contextMenuStrip.SuspendLayout();
            this.buttonsPanel.SuspendLayout();
            this.tabControlPanel.SuspendLayout();
            this.issueTabControl.SuspendLayout();
            this.issueTabPage.SuspendLayout();
            this.issueSearchPanel.SuspendLayout();
            this.previewTabPage.SuspendLayout();
            this.issuePrevCaptionPanel.SuspendLayout();
            this.issuePrevPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyURLToolStripMenuItem,
            this.detailsToolStripMenuItem,
            this.refreshToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(169, 70);
            // 
            // copyURLToolStripMenuItem
            // 
            this.copyURLToolStripMenuItem.Name = "copyURLToolStripMenuItem";
            this.copyURLToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyURLToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.copyURLToolStripMenuItem.Text = "Copy URL";
            this.copyURLToolStripMenuItem.Click += new System.EventHandler(this.CopyURLToolStripMenuItem_Click);
            // 
            // detailsToolStripMenuItem
            // 
            this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
            this.detailsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.detailsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.detailsToolStripMenuItem.Text = "Details";
            this.detailsToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemDetails_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemRefresh_Click);
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.buttonsPanel.Controls.Add(this.companionsComboBox);
            this.buttonsPanel.Controls.Add(this.companionsLabel);
            this.buttonsPanel.Controls.Add(this.okButton);
            this.buttonsPanel.Controls.Add(this.cancelButton);
            this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonsPanel.Location = new System.Drawing.Point(0, 569);
            this.buttonsPanel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(1140, 57);
            this.buttonsPanel.TabIndex = 43;
            // 
            // companionsComboBox
            // 
            this.companionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.companionsComboBox.FormattingEnabled = true;
            this.companionsComboBox.Location = new System.Drawing.Point(88, 19);
            this.companionsComboBox.Name = "companionsComboBox";
            this.companionsComboBox.Size = new System.Drawing.Size(247, 21);
            this.companionsComboBox.TabIndex = 48;
            // 
            // companionsLabel
            // 
            this.companionsLabel.AutoSize = true;
            this.companionsLabel.Location = new System.Drawing.Point(15, 22);
            this.companionsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.companionsLabel.Name = "companionsLabel";
            this.companionsLabel.Size = new System.Drawing.Size(68, 13);
            this.companionsLabel.TabIndex = 47;
            this.companionsLabel.Text = "Companions:";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(974, 13);
            this.okButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 30);
            this.okButton.TabIndex = 39;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(1055, 13);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 30);
            this.cancelButton.TabIndex = 40;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // tabControlPanel
            // 
            this.tabControlPanel.Controls.Add(this.issueTabControl);
            this.tabControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel.Location = new System.Drawing.Point(0, 0);
            this.tabControlPanel.Margin = new System.Windows.Forms.Padding(2);
            this.tabControlPanel.Name = "tabControlPanel";
            this.tabControlPanel.Size = new System.Drawing.Size(1140, 569);
            this.tabControlPanel.TabIndex = 41;
            // 
            // issueTabControl
            // 
            this.issueTabControl.Controls.Add(this.issueTabPage);
            this.issueTabControl.Controls.Add(this.previewTabPage);
            this.issueTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issueTabControl.Location = new System.Drawing.Point(0, 0);
            this.issueTabControl.Margin = new System.Windows.Forms.Padding(2);
            this.issueTabControl.Name = "issueTabControl";
            this.issueTabControl.SelectedIndex = 0;
            this.issueTabControl.Size = new System.Drawing.Size(1140, 569);
            this.issueTabControl.TabIndex = 42;
            this.issueTabControl.SelectedIndexChanged += new System.EventHandler(this.StatusLabel_TextChanged);
            // 
            // issueTabPage
            // 
            this.issueTabPage.Controls.Add(this.filterListView);
            this.issueTabPage.Controls.Add(this.issueListView);
            this.issueTabPage.Controls.Add(this.issueSearchPanel);
            this.issueTabPage.Location = new System.Drawing.Point(4, 22);
            this.issueTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.issueTabPage.Name = "issueTabPage";
            this.issueTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.issueTabPage.Size = new System.Drawing.Size(1132, 543);
            this.issueTabPage.TabIndex = 0;
            this.issueTabPage.Text = "Issue";
            this.issueTabPage.UseVisualStyleBackColor = true;
            // 
            // filterListView
            // 
            this.filterListView.CheckBoxes = true;
            this.filterListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.filterColumnHeader});
            this.filterListView.Dock = System.Windows.Forms.DockStyle.Right;
            this.filterListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.filterListView.Location = new System.Drawing.Point(920, 51);
            this.filterListView.Margin = new System.Windows.Forms.Padding(2);
            this.filterListView.Name = "filterListView";
            this.filterListView.Size = new System.Drawing.Size(210, 490);
            this.filterListView.TabIndex = 47;
            this.filterListView.UseCompatibleStateImageBehavior = false;
            this.filterListView.View = System.Windows.Forms.View.Details;
            // 
            // filterColumnHeader
            // 
            this.filterColumnHeader.Text = "Filters";
            this.filterColumnHeader.Width = 200;
            // 
            // issueListView
            // 
            this.issueListView.CheckBoxes = true;
            this.issueListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idColumn,
            this.titleColumn,
            this.labelsColumn,
            this.assignToColumn,
            this.milestoneColumn,
            this.descriptionColumn,
            this.authorColumn});
            this.issueListView.ContextMenuStrip = this.contextMenuStrip;
            this.issueListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issueListView.FullRowSelect = true;
            this.issueListView.GridLines = true;
            this.issueListView.HideSelection = false;
            this.issueListView.Location = new System.Drawing.Point(2, 51);
            this.issueListView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.issueListView.MultiSelect = false;
            this.issueListView.Name = "issueListView";
            this.issueListView.Size = new System.Drawing.Size(1128, 490);
            this.issueListView.TabIndex = 33;
            this.issueListView.UseCompatibleStateImageBehavior = false;
            this.issueListView.View = System.Windows.Forms.View.Details;
            this.issueListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.IssueListView_ColumnClick);
            this.issueListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.IssueListView_ItemChecked);
            this.issueListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.IssueListView_ItemSelectionChanged);
            // 
            // idColumn
            // 
            this.idColumn.Text = "ID";
            // 
            // titleColumn
            // 
            this.titleColumn.Text = "Title";
            this.titleColumn.Width = 220;
            // 
            // labelsColumn
            // 
            this.labelsColumn.DisplayIndex = 3;
            this.labelsColumn.Text = "Labels";
            this.labelsColumn.Width = 154;
            // 
            // assignToColumn
            // 
            this.assignToColumn.DisplayIndex = 2;
            this.assignToColumn.Text = "Assign to";
            this.assignToColumn.Width = 158;
            // 
            // milestoneColumn
            // 
            this.milestoneColumn.Text = "Milestone";
            this.milestoneColumn.Width = 134;
            // 
            // descriptionColumn
            // 
            this.descriptionColumn.Text = "Description";
            this.descriptionColumn.Width = 250;
            // 
            // authorColumn
            // 
            this.authorColumn.Text = "Author";
            // 
            // issueSearchPanel
            // 
            this.issueSearchPanel.BackColor = System.Drawing.SystemColors.Control;
            this.issueSearchPanel.Controls.Add(this.issueStateComboBox);
            this.issueSearchPanel.Controls.Add(this.issueStateLabel);
            this.issueSearchPanel.Controls.Add(this.regExpCheckBox);
            this.issueSearchPanel.Controls.Add(this.searchComboBox);
            this.issueSearchPanel.Controls.Add(this.searchFieldComboBox);
            this.issueSearchPanel.Controls.Add(this.searchInLabel);
            this.issueSearchPanel.Controls.Add(this.searchLabel);
            this.issueSearchPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.issueSearchPanel.Location = new System.Drawing.Point(2, 2);
            this.issueSearchPanel.Margin = new System.Windows.Forms.Padding(2);
            this.issueSearchPanel.Name = "issueSearchPanel";
            this.issueSearchPanel.Size = new System.Drawing.Size(1128, 49);
            this.issueSearchPanel.TabIndex = 0;
            // 
            // issueStateComboBox
            // 
            this.issueStateComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.issueStateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.issueStateComboBox.FormattingEnabled = true;
            this.issueStateComboBox.Location = new System.Drawing.Point(990, 14);
            this.issueStateComboBox.Name = "issueStateComboBox";
            this.issueStateComboBox.Size = new System.Drawing.Size(134, 21);
            this.issueStateComboBox.TabIndex = 46;
            // 
            // issueStateLabel
            // 
            this.issueStateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.issueStateLabel.AutoSize = true;
            this.issueStateLabel.Location = new System.Drawing.Point(923, 17);
            this.issueStateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.issueStateLabel.Name = "issueStateLabel";
            this.issueStateLabel.Size = new System.Drawing.Size(63, 13);
            this.issueStateLabel.TabIndex = 45;
            this.issueStateLabel.Text = "Issue State:";
            // 
            // regExpCheckBox
            // 
            this.regExpCheckBox.AutoSize = true;
            this.regExpCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.regExpCheckBox.Location = new System.Drawing.Point(335, 15);
            this.regExpCheckBox.Name = "regExpCheckBox";
            this.regExpCheckBox.Size = new System.Drawing.Size(92, 17);
            this.regExpCheckBox.TabIndex = 41;
            this.regExpCheckBox.Text = "using RegExp";
            this.regExpCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchComboBox
            // 
            this.searchComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.searchComboBox.FormattingEnabled = true;
            this.searchComboBox.Location = new System.Drawing.Point(82, 13);
            this.searchComboBox.Name = "searchComboBox";
            this.searchComboBox.Size = new System.Drawing.Size(144, 21);
            this.searchComboBox.TabIndex = 38;
            // 
            // searchFieldComboBox
            // 
            this.searchFieldComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchFieldComboBox.FormattingEnabled = true;
            this.searchFieldComboBox.Location = new System.Drawing.Point(254, 13);
            this.searchFieldComboBox.Name = "searchFieldComboBox";
            this.searchFieldComboBox.Size = new System.Drawing.Size(76, 21);
            this.searchFieldComboBox.TabIndex = 40;
            // 
            // searchInLabel
            // 
            this.searchInLabel.AutoSize = true;
            this.searchInLabel.Location = new System.Drawing.Point(232, 15);
            this.searchInLabel.Name = "searchInLabel";
            this.searchInLabel.Size = new System.Drawing.Size(15, 13);
            this.searchInLabel.TabIndex = 39;
            this.searchInLabel.Text = "in";
            // 
            // searchLabel
            // 
            this.searchLabel.AutoSize = true;
            this.searchLabel.Location = new System.Drawing.Point(18, 17);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(44, 13);
            this.searchLabel.TabIndex = 37;
            this.searchLabel.Text = "&Search:";
            // 
            // previewTabPage
            // 
            this.previewTabPage.Controls.Add(this.issuePrevPanel);
            this.previewTabPage.Controls.Add(this.issuePrevCaptionPanel);
            this.previewTabPage.Location = new System.Drawing.Point(4, 22);
            this.previewTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.previewTabPage.Name = "previewTabPage";
            this.previewTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.previewTabPage.Size = new System.Drawing.Size(1132, 543);
            this.previewTabPage.TabIndex = 1;
            this.previewTabPage.Text = "Preview";
            this.previewTabPage.UseVisualStyleBackColor = true;
            // 
            // issuePrevCaptionPanel
            // 
            this.issuePrevCaptionPanel.BackColor = System.Drawing.SystemColors.Control;
            this.issuePrevCaptionPanel.Controls.Add(this.issuePrevCaptionLabel);
            this.issuePrevCaptionPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.issuePrevCaptionPanel.Location = new System.Drawing.Point(2, 2);
            this.issuePrevCaptionPanel.Name = "issuePrevCaptionPanel";
            this.issuePrevCaptionPanel.Size = new System.Drawing.Size(1128, 49);
            this.issuePrevCaptionPanel.TabIndex = 33;
            // 
            // issuePrevCaptionLabel
            // 
            this.issuePrevCaptionLabel.AutoSize = true;
            this.issuePrevCaptionLabel.Location = new System.Drawing.Point(18, 17);
            this.issuePrevCaptionLabel.Name = "issuePrevCaptionLabel";
            this.issuePrevCaptionLabel.Size = new System.Drawing.Size(49, 13);
            this.issuePrevCaptionLabel.TabIndex = 0;
            this.issuePrevCaptionLabel.Text = "Issue ID:";
            // 
            // issuePrevPanel
            // 
            this.issuePrevPanel.Controls.Add(this.descWebBrowser);
            this.issuePrevPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.issuePrevPanel.Location = new System.Drawing.Point(2, 51);
            this.issuePrevPanel.Name = "issuePrevPanel";
            this.issuePrevPanel.Size = new System.Drawing.Size(1128, 490);
            this.issuePrevPanel.TabIndex = 34;
            // 
            // descWebBrowser
            // 
            this.descWebBrowser.AllowWebBrowserDrop = false;
            this.descWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descWebBrowser.IsWebBrowserContextMenuEnabled = false;
            this.descWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.descWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.descWebBrowser.Name = "descWebBrowser";
            this.descWebBrowser.Size = new System.Drawing.Size(1128, 490);
            this.descWebBrowser.TabIndex = 32;
            // 
            // IssueBrowserDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(1140, 626);
            this.Controls.Add(this.tabControlPanel);
            this.Controls.Add(this.buttonsPanel);
            this.Location = new System.Drawing.Point(-1, -1);
            this.Name = "IssueBrowserDialog";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "IssueBrowserDialog";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IssueBrowserDialog_FormClosed);
            this.Load += new System.EventHandler(this.IssueBrowserDialog_Load);
            this.Shown += new System.EventHandler(this.IssueBrowserDialog_Shown);
            this.contextMenuStrip.ResumeLayout(false);
            this.buttonsPanel.ResumeLayout(false);
            this.buttonsPanel.PerformLayout();
            this.tabControlPanel.ResumeLayout(false);
            this.issueTabControl.ResumeLayout(false);
            this.issueTabPage.ResumeLayout(false);
            this.issueSearchPanel.ResumeLayout(false);
            this.issueSearchPanel.PerformLayout();
            this.previewTabPage.ResumeLayout(false);
            this.issuePrevCaptionPanel.ResumeLayout(false);
            this.issuePrevCaptionPanel.PerformLayout();
            this.issuePrevPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyURLToolStripMenuItem;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.ComboBox companionsComboBox;
        private System.Windows.Forms.Label companionsLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Panel tabControlPanel;
        private System.Windows.Forms.TabControl issueTabControl;
        private System.Windows.Forms.TabPage issueTabPage;
        private System.Windows.Forms.ListView issueListView;
        private System.Windows.Forms.ColumnHeader idColumn;
        private System.Windows.Forms.ColumnHeader titleColumn;
        private System.Windows.Forms.ColumnHeader assignToColumn;
        private System.Windows.Forms.ColumnHeader labelsColumn;
        private System.Windows.Forms.ColumnHeader milestoneColumn;
        private System.Windows.Forms.Panel issueSearchPanel;
        private System.Windows.Forms.ComboBox issueStateComboBox;
        private System.Windows.Forms.Label issueStateLabel;
        private System.Windows.Forms.CheckBox regExpCheckBox;
        private System.Windows.Forms.ComboBox searchComboBox;
        private System.Windows.Forms.ComboBox searchFieldComboBox;
        private System.Windows.Forms.Label searchInLabel;
        private System.Windows.Forms.Label searchLabel;
        private System.Windows.Forms.TabPage previewTabPage;
        private System.Windows.Forms.ColumnHeader descriptionColumn;
        private System.Windows.Forms.ColumnHeader authorColumn;
        private System.Windows.Forms.ListView filterListView;
        private System.Windows.Forms.ColumnHeader filterColumnHeader;
        private System.Windows.Forms.Panel issuePrevPanel;
        private System.Windows.Forms.WebBrowser descWebBrowser;
        private System.Windows.Forms.Panel issuePrevCaptionPanel;
        private System.Windows.Forms.Label issuePrevCaptionLabel;
    }
}