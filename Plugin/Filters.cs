﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GitLabApiClient.Models.Issues.Responses;

namespace TIssuePlugin
{
    internal interface IFilter
    {
        bool IsValid(Issue issue);
    }

    internal class Filters : ObservableCollection<IFilter>
    {
        public IEnumerable<Issue> Filter(IEnumerable<Issue> issues)
        {
            foreach (var issue in issues)
            {
                if (!Items.Select(x => x.IsValid(issue)).Any(x => x == false))
                {
                    yield return issue;
                }
            }
        }
    }

    internal class Filter<T> : IFilter
    {
        readonly T _value;
        Func<T, Issue, bool> _searchFunc;
        public Filter(T value, Func<T, Issue, bool> searchFunc)
        {
            _value = value;
            _searchFunc = searchFunc;
        }

        public bool IsValid(Issue issue)
        {
            return _searchFunc(_value, issue);
        }
    }
}