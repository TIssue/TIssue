﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TIssuePlugin
{
    internal partial class RemoteSelectorDialog : Form
    {
        private RemoteSelectorDialog()
        {
            InitializeComponent();
        }

        public static string GetRemoteName(string[] remotes, IntPtr parentWnd)
        {
            var dialog = new RemoteSelectorDialog();
            var nativeWindow = new NativeWindow();
            nativeWindow.AssignHandle(parentWnd);

            dialog.remotesComboBox.Items.AddRange(remotes);
            dialog.remotesComboBox.SelectedIndex = 0;

            if (dialog.ShowDialog(nativeWindow) == DialogResult.OK)
                return dialog.remotesComboBox.SelectedItem.ToString();

            return "";
        }

        private void RemoteSelectorDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }
}
