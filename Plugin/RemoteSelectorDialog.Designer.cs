﻿namespace TIssuePlugin
{
    partial class RemoteSelectorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.oKbutton = new System.Windows.Forms.Button();
            this.remotesComboBox = new System.Windows.Forms.ComboBox();
            this.gitLabPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gitLabPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // oKbutton
            // 
            this.oKbutton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.oKbutton.Location = new System.Drawing.Point(270, 12);
            this.oKbutton.Name = "oKbutton";
            this.oKbutton.Size = new System.Drawing.Size(75, 23);
            this.oKbutton.TabIndex = 1;
            this.oKbutton.Text = "OK";
            this.oKbutton.UseVisualStyleBackColor = true;
            // 
            // remotesComboBox
            // 
            this.remotesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.remotesComboBox.FormattingEnabled = true;
            this.remotesComboBox.Location = new System.Drawing.Point(71, 14);
            this.remotesComboBox.Name = "remotesComboBox";
            this.remotesComboBox.Size = new System.Drawing.Size(184, 21);
            this.remotesComboBox.TabIndex = 0;
            // 
            // gitLabPictureBox
            // 
            this.gitLabPictureBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.gitLabPictureBox.Image = global::TIssuePlugin.Properties.Resources.logo;
            this.gitLabPictureBox.Location = new System.Drawing.Point(0, 0);
            this.gitLabPictureBox.Name = "gitLabPictureBox";
            this.gitLabPictureBox.Size = new System.Drawing.Size(55, 45);
            this.gitLabPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.gitLabPictureBox.TabIndex = 3;
            this.gitLabPictureBox.TabStop = false;
            // 
            // RemoteSelectorDialog
            // 
            this.AcceptButton = this.oKbutton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 45);
            this.Controls.Add(this.gitLabPictureBox);
            this.Controls.Add(this.remotesComboBox);
            this.Controls.Add(this.oKbutton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "RemoteSelectorDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select remotes";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RemoteSelectorDialog_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.gitLabPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button oKbutton;
        private System.Windows.Forms.ComboBox remotesComboBox;
        private System.Windows.Forms.PictureBox gitLabPictureBox;
    }
}