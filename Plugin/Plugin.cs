﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GitLabApiClient;
using Interop.BugTraqProvider;
using LibGit2Sharp;
using System.Linq;

namespace TIssuePlugin
{
    [ComVisible(true)]
#if x64
    [Guid("AE3068AD-2498-45D9-BA61-6AD6F1FD3A1D")]
#else
    [Guid("13CAF71C-3FC1-4EA6-B257-A85C26187EED")]
#endif
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("TIssue.Plugin")]
    public class Plugin : IBugTraqProvider2
    {
        private static IEnumerable<IGitLabInfo> GetGitlabInfos()
        {
            var gitlabInfos = new List<IGitLabInfo>();

            var configuration = Configuration.BuildFrom(null);

            var configSections = configuration.Find(@"gitlab\..+\.token", ConfigurationLevel.Global);

            foreach (ConfigurationEntry<string> configSection in configSections)
            {
                var webUrl = configSection.Key.Substring(7, configSection.Key.Length - 13);

                gitlabInfos.Add(new GitLabInfo
                {
                    WebUrl = webUrl,
                    Token = configSection.Value,
                    CloneUrl = configuration.GetValueOrDefault(configSection.Key.Replace("token", "ssh"), webUrl)
                });
            }

            return gitlabInfos;
        }

        protected static Match ParseHeadRemoteUrl(string remoteUrl)
        {
            return new Regex(@"(?:\w+@|\w+://)(?<host>(?:\w|\.|-)+)[:/](?<path>(?<namespace>[\w/-]+)/(?<name>[\w-]+))(?:[^/]*)+$").Match(remoteUrl);
        }

        protected static GitLabClient CreateGitLabClient(string host)
        {
            if (!string.IsNullOrEmpty(host))
                foreach (IGitLabInfo gitLabInfo in GetGitlabInfos())
                {
                    if (string.Equals(gitLabInfo.CloneUrl, host, StringComparison.OrdinalIgnoreCase))
                        return new GitLabClient(new UriBuilder(gitLabInfo.WebUrl).Uri.ToString(), gitLabInfo.Token);
                }

            throw new NotFoundException($"GitLab info not found for host \"{host}\"");
        }

        public Plugin()
        {
            NetPatch.LeaveDotsAndSlashesEscaped();

            // remove delay at connection with a server
            WebRequest.DefaultWebProxy = new WebProxy();

#if DEBUG
            if (!Debugger.IsAttached)
            {
                Debugger.Launch();
            }
            else
            {
                //Debugger.Break();
            }
#endif
        }

        public bool ValidateParameters(IntPtr hParentWnd, string parameters)
        {
            return true;
        }

        public string GetLinkText(IntPtr hParentWnd, string parameters)
        {
            // Text of button
            return "Issue";
        }

        public string GetCommitMessage(IntPtr hParentWnd, string parameters, string commonRoot, string[] pathList, string originalMessage)
        {
            return originalMessage;
        }

        public string GetCommitMessage2(IntPtr hParentWnd, string parameters, string commonURL, string commonRoot, string[] pathList, string originalMessage, string bugID, out string bugIDOut, out string[] revPropNames, out string[] revPropValues)
        {
            bugIDOut = bugID;
            revPropNames = new string[] { };
            revPropValues = new string[] { };

            try
            {
                var remoteUrl = commonURL;
                var companions = parameters;

                if (Directory.Exists(remoteUrl))
                {
                    var repository = new Repository(remoteUrl);

                    var remoteName = repository.Head.RemoteName;

                    if (remoteName == null && repository.Network.Remotes.Any())
                        if (repository.Network.Remotes.Count() == 1)
                            remoteName = repository.Network.Remotes.ElementAt(0).Name;
                        else
                            remoteName = RemoteSelectorDialog.GetRemoteName(repository.Network.Remotes.Select(i => i.Name).ToArray(), hParentWnd);

                    if (string.IsNullOrEmpty(remoteName))
                        return originalMessage;

                    remoteUrl = repository.Network.Remotes[remoteName].Url;
                    companions = repository.Config.GetValueOrDefault($"remote.{remoteName}.companions", ConfigurationLevel.Local, "");
                }

                var parsedUrl = ParseHeadRemoteUrl(remoteUrl);

                var gitLabClient = CreateGitLabClient(parsedUrl.Groups["host"].Value);

                var gitLabProjects = new List<string>();
                gitLabProjects.Add(parsedUrl.Groups["path"].Value);
                if (!string.IsNullOrEmpty(companions))
                    gitLabProjects.AddRange(companions.Split('*'));

                var selectedIssues = IssueBrowserDialog.SelectIssues(gitLabClient, gitLabProjects, hParentWnd);
                if (selectedIssues != "")
                {
                    originalMessage = (string.IsNullOrEmpty(originalMessage) ? "Closes " : originalMessage) + selectedIssues;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Control.FromHandle(hParentWnd), ex.Message,  "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return originalMessage;
        }

        public string CheckCommit(IntPtr hParentWnd, string parameters, string commonURL, string commonRoot, string[] pathList, string commitMessage)
        {
            return "";
        }

        public string OnCommitFinished(IntPtr hParentWnd, string commonRoot, string[] pathList, string logMessage, int revision)
        {
            return logMessage;
        }

        public bool HasOptions()
        {
            return false;
        }

        public string ShowOptionsDialog(IntPtr hParentWnd, string parameters)
        {
            throw new NotImplementedException();
        }
    }
}