﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using GitLabApiClient;
using GitLabApiClient.Models.Issues.Responses;
using TIssuePlugin.Properties;

namespace TIssuePlugin
{
    internal partial class IssueBrowserDialog : Form
    {
        private readonly GitLabClient _gitLabClient;
        private readonly IEnumerable<string> _gitLabProjects;

        private readonly ListViewColumnSorter _listViewSorter;
        private readonly Dictionary<string, IIssueProperty> _searchFields;
        private readonly List<Issue> _issues = new List<Issue>();

        private string CurrentProject
        {
            get { return companionsComboBox.SelectedItem.ToString(); }
        }

        private IssueState CurrentState
        {
            get { return issueStateComboBox.SelectedItem != null ? (IssueState)issueStateComboBox.SelectedIndex : IssueState.All; }
        }

        /// <summary>
        /// Displays form to allow user to select issues.
        /// </summary>
        public static string SelectIssues(GitLabClient gitLabClient, IEnumerable<string> gitLabProjects, IntPtr parentWnd)
        {
            var dialog = new IssueBrowserDialog(gitLabClient, gitLabProjects);

            var nativeWindow = new NativeWindow();
            nativeWindow.AssignHandle(parentWnd);

            var currentProject = "";

            if (dialog.ShowDialog(nativeWindow) == DialogResult.OK)
            {
                if (dialog.companionsComboBox.SelectedIndex > 0)
                {
                    currentProject = dialog.CurrentProject;
                    var parentNamespace = currentProject.Substring(0, currentProject.LastIndexOf('/'));
                    var adoptNamespace = dialog.companionsComboBox.Items[0].ToString().Substring(0, dialog.companionsComboBox.Items[0].ToString().LastIndexOf('/'));

                    if (parentNamespace == adoptNamespace)
                        currentProject = currentProject.Substring(parentNamespace.Length + 1);
                }

                return string.Join(", ", dialog.GetCheckedIssues().OrderBy(id => id).Select(id => $"{currentProject}#" + id.ToString()));
            }
            else
            {
                return "";
            }
        }
        private IssueBrowserDialog(GitLabClient gitLabClient, IEnumerable<string> gitLabProjects)
        {
            InitializeComponent();

            _gitLabClient = gitLabClient;
            _gitLabProjects = gitLabProjects;

            _listViewSorter = new ListViewColumnSorter();
            issueListView.ListViewItemSorter = _listViewSorter;
            _listViewSorter.SortColumn = 0;
            _listViewSorter.Order = SortOrder.Ascending;

            _searchFields = new Dictionary<string, IIssueProperty>()
            {
                {"All fields", new IssueProperty((i) => (string.Join("?", from t in _searchFields where !string.Equals(t.Key, "All fields", StringComparison.CurrentCultureIgnoreCase) select t.Value.AsString(i))))},
                {"ID", new IssueProperty((i) => i.Iid.ToString())},
                {"Title", new IssueProperty((i) => i.Title)},
                {"Labels", new IssueProperty((i) => string.Join(",", i.Labels), (i) => i.Labels.ToArray())},
                {"Assign to", new IssueProperty((i) => i.Assignee != null ? i.Assignee.Name : "", (i) => i.Assignee != null ?  new[]{ i.Assignee.Name } : Array.Empty<string>())},
                {"Milestone", new IssueProperty((i) => i.Milestone != null ? i.Milestone.Title : "", (i) => i.Milestone != null ?  new[]{ i.Milestone.Title } : Array.Empty<string>())},
                {"Description", new IssueProperty((i) => i.Description)},
                {"Author", new IssueProperty((i) => i.Author != null ? i.Author.Name : "")}
            };

            InitializeItem();

            ReloadIssue();
        }

        private void InitializeItem()
        {
            foreach (var item in _searchFields.Where(f => f.Value.GetFilterValues != null))
            {
                filterListView.Groups.Add(new ListViewGroup(item.Key, item.Key));
            }

            companionsComboBox.Items.Clear();
            companionsComboBox.Items.AddRange(_gitLabProjects.ToArray());

            issueStateComboBox.Items.Clear();
            issueStateComboBox.Items.AddRange(Enum.GetNames(typeof(IssueState)));

            searchFieldComboBox.Items.AddRange(_searchFields.Keys.ToArray());

            companionsComboBox.SelectedIndex = 0;
            issueStateComboBox.SelectedItem = IssueState.Opened.ToString();
            searchFieldComboBox.SelectedIndex = 0;

            filterListView.ItemChecked += new ItemCheckedEventHandler(this.FilterListView_ItemChecked);
            companionsComboBox.SelectedIndexChanged += new EventHandler(this.ToolStripMenuItemRefresh_Click);
            issueStateComboBox.SelectedIndexChanged += new EventHandler(this.ToolStripMenuItemRefresh_Click);
            searchComboBox.TextChanged += new EventHandler(this.SearchBox_TextChanged);
            searchFieldComboBox.SelectedIndexChanged += new EventHandler(this.SearchBox_TextChanged);
        }

        private IEnumerable<int> GetCheckedIssues()
        {
            return (from item in issueListView.CheckedItems.OfType<ListViewItem>() select ((Issue)item.Tag).Iid).ToArray();
        }

        private void UpdateIssuesFilter()
        {
            filterListView.Items.Clear();

            var collectionItem = new List<ListViewItem>();

            foreach (var field in _searchFields.Where(f => f.Value.GetFilterValues != null))
            {
                var group = filterListView.Groups[field.Key];

                foreach (var value in _issues.SelectMany(i => field.Value.GetFilterValues(i)).Distinct().OrderBy(s => s))
                {
                    collectionItem.Add(new ListViewItem(value, group));
                }
            }

            filterListView.Items.AddRange(collectionItem.ToArray());
        }

        private void ReloadIssue()
        {
            _issues.Clear();
            var projectId = CurrentProject.Replace("/", "%2F");

            //Здесь по условию был вызов без CurrentState, но в 1.0.8 он возвращает не понятно что
            var issues = Task.Run(async () => await _gitLabClient.Issues.GetAsync(projectId, i => i.State = CurrentState)).Result;
            _issues.AddRange(issues);
            //_issues.AddRange(await _gitLabClient.Issues.GetAsync(projectId, i => i.State = CurrentState));

            UpdateIssuesFilter();
            FillListView();
        }

        private void FillListView()
        {
            var filters = new Filters();

            if (!string.IsNullOrEmpty(searchComboBox.Text))
            {
                Func<string, Issue, bool> searchFunc;

                if (!regExpCheckBox.Checked)
                    searchFunc = (searchText, issue) => _searchFields[searchFieldComboBox.Text].AsString(issue).IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) > -1;
                else if (RegexUtils.VerifyRegEx(searchComboBox.Text))
                    searchFunc = (searchText, issue) => Regex.IsMatch(_searchFields[searchFieldComboBox.Text].AsString(issue), searchText);
                else
                    searchFunc = (searchText, issue) => false;

                filters.Add(new Filter<string>(searchComboBox.Text, searchFunc));
            }

            foreach (ListViewGroup filterGroup in filterListView.Groups)
            {
                var filterGroupItems = filterGroup.Items.Cast<ListViewItem>().ToArray();
                var filterValues = filterGroupItems.Where(i => i.Checked).Select(i => i.Text).ToArray();
                //var filterValues = filterGroup.Items.Cast<ListViewItem>().Where(i => i.Checked).Select(i => i.Text).ToArray();

                if (filterValues.Any())
                    filters.Add(new Filter<string[]>(filterValues, _searchFields[filterGroup.Name].InList));
            }

            var issues = _issues;
            if (filters.Any())
                issues = filters.Filter(_issues).ToList();

            issueListView.BeginUpdate();
            issueListView.Items.Clear();

            foreach (var issue in issues)
            {
                var listViewitem = new ListViewItem(_searchFields[issueListView.Columns[0].Text].AsString(issue));
                issueListView.Items.Add(listViewitem);
                listViewitem.Tag = issue;

                if (issue.State == IssueState.Closed)
                    listViewitem.ForeColor = SystemColors.ButtonShadow;

                foreach (var col in (from ColumnHeader column in issueListView.Columns.OfType<ColumnHeader>() where column.Index > 0 select column))
                {
                    listViewitem.SubItems.Insert(col.Index, new ListViewItem.ListViewSubItem(listViewitem, _searchFields[col.Text].AsString(issue)));
                }
            }
            issueListView.EndUpdate();

            issueListView.Sort();

            if (issueListView.Items.Count != 0)
                issueListView.Items[0].Selected = true;
            else
                UpdateControlStates();
        }

        private void UpdateControlStates()
        {
            okButton.Enabled = issueListView.CheckedItems.Count > 0;
            detailsToolStripMenuItem.Enabled = issueListView.SelectedItems.Count == 1;

            companionsComboBox.Enabled = issueTabControl.SelectedTab == issueTabPage;

            refreshToolStripMenuItem.Enabled = companionsComboBox.Enabled;

            Text = $"{CurrentProject}: {issueListView.Items.Count} issue(s)";

            if (issueListView.SelectedItems.Count == 0)
                issuePrevCaptionLabel.Text = "No Issue";
            else
            {
                var issue = (issueListView.SelectedItems[0].Tag as Issue);
                issuePrevCaptionLabel.Text = $"Issue #{issue.Iid}: {issue.Title}";
            }
        }

        protected string GetSelectedIssueLink()
        {
            return string.Format("http://{0}/{1}/issues/{2}", new UriBuilder(_gitLabClient.HostUrl).Host, CurrentProject, issueListView.SelectedItems[0].Text);
        }

        private void StatusLabel_TextChanged(object sender, EventArgs e)
        {
            var mdText = "";

            if (issueListView.SelectedItems.Count > 0)
            {
                mdText = CommonMark.CommonMarkConverter.Convert(((Issue)issueListView.SelectedItems[0].Tag).Description);

                mdText = $"<div style=\"font-family:Verdana;font-size:10pt\">\n{mdText}\n</div>";
            }
            //insert url to images path (absolute path for WebBrowser)
            mdText = mdText.Replace(@"src=""/", @"src=""" + CurrentProject + "/");
            descWebBrowser.DocumentText = mdText;

            UpdateControlStates();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            FillListView();
        }

        private void IssueListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            UpdateControlStates();
        }

        private void IssueListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == _listViewSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                _listViewSorter.Order = _listViewSorter.Order == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _listViewSorter.SortColumn = e.Column;
                _listViewSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            issueListView.Sort();
        }

        private void IssueBrowserDialog_Shown(object sender, EventArgs e)
        {
            // patch issue description diaplaing in a browser
            UpdateControlStates();
        }

        private void ToolStripMenuItemRefresh_Click(object sender, EventArgs e)
        {
              ReloadIssue();
        }

        private void ToolStripMenuItemDetails_Click(object sender, EventArgs e)
        {
            Process.Start(GetSelectedIssueLink());
        }

        private void CopyURLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(GetSelectedIssueLink());
        }

        private void FilterListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (filterListView.FocusedItem != null) // проверяет на непрограммное нажатие
              FillListView();
        }

        private void IssueListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
             UpdateControlStates();
        }

        private void IssueBrowserDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.Default.IssueBrowserDialogWindowState = WindowState;

            if (WindowState == FormWindowState.Normal)
            {
                Settings.Default.IssueBrowserDialogLocation = Location;
                Settings.Default.IssueBrowserDialogSize = Size;
            }

            Settings.Default.Save();
        }

        private void IssueBrowserDialog_Load(object sender, EventArgs e)
        {
            Location = Settings.Default.IssueBrowserDialogLocation;
            Size = Settings.Default.IssueBrowserDialogSize;
            WindowState = Settings.Default.IssueBrowserDialogWindowState;

            var location = Settings.Default.IssueBrowserDialogLocation;

            if (location.X < 0 || location.Y < 0)
            {
                StartPosition = FormStartPosition.CenterParent;
            }
        }
    }
}
