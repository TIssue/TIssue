﻿using System;
using System.Linq;
using GitLabApiClient.Models.Issues.Responses;

namespace TIssuePlugin
{
    interface IGitLabInfo
    {
        string WebUrl { get; }
        string Token { get; }
        string CloneUrl { get; }
    }

    internal interface IIssueProperty
    {
        Func<Issue, string[]> GetFilterValues { get; }
        string AsString(Issue issue);
        bool InList(string[] filterValues, Issue issue);

        string[] FilterValues(Issue issue);
    }

    internal class GitLabInfo : IGitLabInfo
    {
        public string WebUrl { get; set; }

        public string Token { get; set; }

        public string CloneUrl { get; set; }
    }

    internal class IssueProperty : IIssueProperty
    {
        private readonly Func<Issue, string> _asString;

        public Func<Issue, string[]> GetFilterValues { get; }

        public IssueProperty(Func<Issue, string> asString, Func<Issue, string[]> getFilterValues = null)
        {
            _asString = asString;
            GetFilterValues = getFilterValues;
        }

        string IIssueProperty.AsString(Issue issue)
        {
            return _asString(issue);
        }

        bool IIssueProperty.InList(string[] filterValues, Issue issue)
        {
            var values = GetFilterValues(issue);

            return (GetFilterValues == null) || values.Any() && (filterValues.Except(values).Count() == 0);
        }

        string[] IIssueProperty.FilterValues(Issue issue)
        {
            if (GetFilterValues != null)
                return (GetFilterValues(issue));

            return null;
        }
    }
}
