﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TIssueViewer
{
    static class Program
    {
        private static string _selectedPath;
        private static string _parameters;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var ac = new ApplicationContext();

            var topMost = new Form()
            {
                StartPosition = FormStartPosition.CenterScreen,
                ShowInTaskbar = true,
                Visible = false,
                FormBorderStyle = FormBorderStyle.None,
                Height = 0,
                Width = 0,
                Opacity = 0
            };

            topMost.Shown += new System.EventHandler(TopMost_Shown);

            _selectedPath = "";
            _parameters = "";

            if (args.Any())
            {
                _selectedPath = args[0];
                _parameters = args.Length > 1 ? args[1] : "";
            }
            else
                using (var dialog = new FolderBrowserDialog())
                {
                    dialog.Description = "Select repository";
                    dialog.SelectedPath = Properties.Settings.Default.SelectedPath;

                    dialog.ShowNewFolderButton = false;
                    if (dialog.ShowDialog(topMost) == DialogResult.OK && !string.IsNullOrEmpty(dialog.SelectedPath))
                    {
                        _selectedPath = dialog.SelectedPath;

                        Properties.Settings.Default.SelectedPath = _selectedPath;
                        Properties.Settings.Default.Save();
                    }
                };

            if (!string.IsNullOrEmpty(_selectedPath))
                Application.Run(topMost);
        }

        private static void TopMost_Shown(object sender, EventArgs e)
        {
            var topMost = ((Form)sender);

            dynamic plugin = Activator.CreateInstance(Type.GetTypeFromProgID("TIssue.Plugin"));

            string bugIDOut = null;
            string[] revPropNames;
            string[] revPropValues;

            plugin.GetCommitMessage2(topMost.Handle, "", _selectedPath, _parameters, null, "", "", out bugIDOut,
                out revPropNames, out revPropValues);

            topMost.Close();
        }
    }
}
