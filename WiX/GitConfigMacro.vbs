Sub GitConfigTokenRead()
	strCommand = Session.Property("GIT_PATH") + "bin\git.exe config --global --get-regexp GitLab\..+\.TOKEN"
	Session.Property("EXISTING_TOKEN") = GitExecute(strCommand)
End Sub

Sub GitConfigTokenWrite()
	GitConfigWrite "GitLab.""" + Session.Property("SERVERNAME") + """", "token", Session.Property("TOKEN")
End Sub

Sub GitConfigGUIDWrite()
	GitConfigRemove "bugtraq", Session.Property("BUGTRAQ_KEY")
	GitConfigWrite "bugtraq", Session.Property("BUGTRAQ_KEY"), Session.Property("BUGTRAQ_VALUE")
End Sub

Sub GitConfigProviderParamsRemove()
	GitConfigRemove "bugtraq", "providerparams"
End Sub

'
' ^^^ for EXTERNAL call
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' vvv for INTERNAL use
'

Sub GitConfigWrite(strSection, strKey, strValue)
	GitExecute Session.Property("GIT_PATH") + "bin\git.exe config --global --add " + strSection + "." + strKey + " " + strValue
End Sub

Sub GitConfigRemove(strSection, strKey)
    strCommandBegin = Session.Property("GIT_PATH") + "bin\git.exe config --global "
    
    ' check if target key presents
    strOutput = GitExecute(strCommandBegin + "--get-regexp " + strSection + "." + strKey)
    If 0 <> Len(strOutput) Then
	    GitExecute strCommandBegin + "--unset " + strSection + "." + strKey
    End If

	' check for empty section
	strOutput = GitExecute(strCommandBegin + "--get-regexp " + strSection + "\..+")
	If 0 = Len(strOutput) Then
		' remove section
		GitExecute strCommandBegin + "--remove-section " + strSection
	End If
End Sub

Function GitExecute(strCommand)
	Set objShell = CreateObject("WScript.Shell")
	Set objExec = objShell.Exec(strCommand)
	
	strOutput = ""
	Do while Not objExec.StdOut.AtEndOfStream
		strOutput = strOutput + objExec.StdOut.ReadLine()
	Loop
	
	GitExecute = strOutput
End Function