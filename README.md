TIssue - The TortoiseGit coolest issue tracker plugin for GitLab
================================================================

1. Description
--------------
TIssue is free open-source the TortoiseGit issue tracker plugin for GitLab. TIssue allows to receive issues from an remote repository such as Gitlab. TIssue supports only IBugtraqProvider2 interface.

2. Requirements
---------------
- TortoiseGit 1.6 and above.  

3. Installation
---------------
TIssue comes with an easy to use installer. Double click on the installer file and follow the instructions. TIssue will be registered automatically. The information about GitLab is added to global git-config file. The installer executed then following command.

```
git config --global Gitlab."<GITLAB_SERVERNAME>".TOKEN <YOUR_TOKEN>
```

4. Registration
---------------
You may to register TIssue manually. Use TortoiseGit for custom registration. Provider UUID of 32-bit plugin version is {13CAF71C-3FC1-4EA6-B257-A85C26187EED}, 64-bit is {AE3068AD-2498-45D9-BA61-6AD6F1FD3A1D}. 